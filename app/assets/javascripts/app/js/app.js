'use strict';

// Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/view1', {templateUrl: '/assets/app/partials/admin.html', controller: AdminController});
    $routeProvider.otherwise({redirectTo: '/page1'});
  }]);
