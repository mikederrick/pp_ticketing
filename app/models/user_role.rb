class UserRole < ActiveRecord::Base
  has_many :user

  scope :default, -> { where(default: true) }

  def is_admin?
    return name.eql?("ADMIN")
  end 
end
