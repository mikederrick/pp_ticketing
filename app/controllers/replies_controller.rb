# Brimir is a helpdesk system to handle email support requests.
# Copyright (C) 2012-2014 Ivaldi http://ivaldi.nl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class RepliesController < ApplicationController

  def create
    puts reply_params.inspect
    @reply = Reply.new(reply_params)

    @reply.user = current_user

    respond_to do |format|
      if @reply.save
        format.html { redirect_to @reply.ticket, notice: 'Reply was successfully created.' }
        format.json { render json: @reply.as_json, status: :created }
        format.js { render }
      else
        format.html { render action: 'new' }
        format.json { render json: @reply.errors, status: :unprocessable_entity }
        format.js { render }
      end
    end
  end

  def approve
    @reply = Reply.find(params[:id])

    respond_to do |format|
      if approve_reply(@reply)
        format.html {
          redirect_to @reply.ticket, notice: 'Question was successfully answered.'
        }
        format.js {
          render notice: 'Question was successfully answered.'
        }
        format.json {
          render json: @reply.ticket.as_json
        }
      else
        format.html {
          render action: 'edit'
        }
        format.json {
          render json: @reply.ticket.errors, status: :unprocessable_entity
        }
      end
    end
  end

  def destroy
    @reply = Reply.find(params[:id])

    respond_to do |format|
      if current_user.user_role.is_admin? && @reply.destroy!
        format.html {
          redirect_to @reply.ticket, notice: 'Reply was successfully destroyed'
        }
        format.js {
          render notice: 'Reply was successfully destroyed'
        }
        format.json {
          render json: @reply.ticket
        }
      else
        format.html {
          render action: 'edit'
        }
        format.json {
          render json: @reply.ticket.errors, status: :unprocessable_entity
        }
      end
    end
  end

  def destroy_reply
    @reply = Reply.find(params[:id])
    @reply.destroy!
    respond_to do |format|
      format.json {
        render json: {}
      }
    end
  end

  def flag_reply
    @reply = Reply.find(params[:id])
    @reply.destroy!
    respond_to do |format|
      format.json {
        render json: {}
      }
    end
  end

  def new
    @reply = Reply.new(reply_params)

    @reply.to = @reply.ticket.user.email
  end

  private
    def reply_params
      params.require(:reply).permit(:message, :ticket_id, :message_id, :user_id,
          :attachments_attributes, :to, :cc, :bcc)
    end

    def approve_reply(reply)
      update_ticket = reply.ticket.update_attributes(status: Status.find_by_name('Closed'))
      update_reply = reply.update_attributes(answer: true)
      post = RestClient.post "#{admin_config['host']}/messages/#{reply.ticket.text_reference}/answer", body: reply.message
      update_ticket && update_reply && post.code == 200
    end

    def admin_config
      YAML::load(IO.read(File.join(Rails.root, 'config', 'admin.yml')))[Rails.env]
    end

end
