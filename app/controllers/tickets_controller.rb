# Brimir is a helpdesk system to handle email support requests.
# Copyright (C) 2012-2014 Ivaldi http://ivaldi.nl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class TicketsController < ApplicationController
  before_filter :authenticate_user!, except: [ :create ] 

  def show
    @ticket = Ticket.find(params[:id])
    # @agents = User.agents
    @statuses = Status.all
    # @priorities = Priority.all    

    @reply = @ticket.replies.new
    @is_admin ||= current_user.user_role.is_admin?
    @active_status = Status.find_by_id_from_filters(params[:status_id], @is_admin)
   # @reply.to = @ticket.user.email
  end

  def index
    @agents = User.agents
    @is_admin ||= current_user.user_role.is_admin?
    @statuses = if @is_admin
      Status.filters.select { |s| s.name != 'Deleted' }
    else 
      Status.filters.select { |s| s.name == 'Open' || s.name == 'Closed' }
    end

    @priorities = Priority.all

    @active_status = Status.find_by_id_from_filters(params[:status_id], current_user.user_role.is_admin?)
    # @tickets = @active_status
    #   .tickets
    #   .search(params[:q])
    #   .page(params[:page])
    #   .order(:created_at)
    @tickets = @active_status.tickets
    puts @tickets.first.inspect
    respond_to do |format|
      format.html {
        render action: 'index'
      }
      format.json {
        render :json => @tickets.as_json
      }  
    end  
  end

  def all
    @tickets = Ticket.all
    respond_to do |format|
      format.html {
        render action: 'index'
      }
      format.json {
        render :json => @tickets.as_json
      }  
    end      
  end

  def update
    @ticket = Ticket.find(params[:id])

    respond_to do |format|
      if current_user.user_role.is_admin? && @ticket.update_attributes(params.permit![:ticket])
        format.html {
          redirect_to tickets_path(status_id: Status.find_by_id_from_filters(params[:status_id]).id), notice: 'Question was successfully updated.'
        }
        format.js {
          render notice: 'Question was succesfully updated.'
        }
        format.json {
          head :no_content
        }
      else
        format.html {
          render action: 'edit'
        }
        format.json {
          render json: @ticket.errors, status: :unprocessable_entity
        }
      end
    end
  end

  def approve
    @ticket = Ticket.find(params[:id])

    respond_to do |format|
      if current_user.user_role.is_admin? && @ticket.update_attributes(status: Status.find_by_name('Open'))
        format.html {
          redirect_to tickets_path(status_id: Status.find_by_id_from_filters(params[:status_id]).id), notice: 'Question was successfully updated.'
        }
        format.js {
          render notice: 'Question was succesfully updated.'
        }
        format.json {
          render json: @ticket.as_json
        }
      else
        format.html {
          render action: 'edit'
        }
        format.json {
          render json: @ticket.errors, status: :unprocessable_entity
        }
      end
    end
  end

  def delete_ticket
    @ticket = Ticket.find(params[:id]).update_attribute(:status, Status.find_by_name('Deleted'))
    respond_to do |format|
      format.json {
        render :json => {}
      }
    end
  end

  def flag_ticket
    @ticket = Ticket.find(params[:id]).update_attribute(:status, Status.find_by_name('Flagged'))
    respond_to do |format|
      format.json {
        render :json => {}
      }
    end
  end

  def best_ticket
    @ticket = Ticket.find(params[:id]).update_attribute(:status, Status.find_by_name('Best'))
    respond_to do |format|
      format.json {
        render :json => {}
      }
    end
  end

  def create

    @ticket = Ticket.create({message: params[:message], status: Status.default.first, text_reference: params[:text_reference]})

    respond_to do |format|
      format.json { render json: @ticket.as_json, status: :created }
    end
  end

  private
    def ticket_params
      params.require(:message).permit!
    end
end
