Brimir::Application.routes.draw do

  resources :replies, only: [ :create, :new, :destroy ]
  post 'replies/:id/approve' => 'replies#approve', as: 'reply_approve'

  devise_for :users
  devise_scope :user do
    post 'login' => 'sessions#create', :as => 'login'
    post 'logout' => 'sessions#destroy', :as => 'logout'
    get 'current_user' => 'sessions#show_current_user', :as => 'show_current_user'
  end

  resources :users, only: [ :edit, :update ]

  resources :tickets, only: [ :index, :show, :update, :create ]
  post 'tickets/:id/approve' => 'tickets#approve', as: 'ticket_approve'
  post 'tickets/:id/delete', :to => 'tickets#delete_ticket', as: 'delete_ticket'
  post 'tickets/:id/flag', :to => 'tickets#flag_ticket'
  post 'tickets/:id/best', :to => 'tickets#best_ticket'
  get 'all_tickets', :to => 'tickets#all'
  
  post 'replies/:id/delete', :to => 'replies#destroy_reply'
  post 'replies/:id/flag', :to => 'replies#flag_reply'

  root :to => 'tickets#index'

  resources :admins, only: [ :index]

  get '/resources' => 'pages#resources'

end
