class AddDestroyedAtToReply < ActiveRecord::Migration
  def change
    add_column :replies, :destroyed_at, :datetime
  end
end
