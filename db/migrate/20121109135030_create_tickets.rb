class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      # t.string :from
      # t.string :subject
      t.text :message
      t.text :text_reference

      t.timestamps
    end
  end
end
