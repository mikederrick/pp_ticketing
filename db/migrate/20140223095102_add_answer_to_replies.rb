class AddAnswerToReplies < ActiveRecord::Migration
  def change
    add_column :replies, :answer, :boolean, default: false
  end
end
