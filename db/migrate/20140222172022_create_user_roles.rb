class CreateUserRoles < ActiveRecord::Migration
  def change
    create_table :user_roles do |t|
      t.string :name
      
      change_table :users do |t|
        t.integer :user_role_id
      end
      add_index :users, :user_role_id
      t.timestamps
    end
  end
end
